﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerSpawner : MonoBehaviour {

	public GameObject Destroyers;
	public Vector3 SpawnZone;
	public float minX, maxX, MinZ, MaxZ;

	public float initialStart;
	public float waitBetweenWaves;

	public int hazardCount;



	void Start()
	{
		StartCoroutine(SpawnDestroyers());

	}

	IEnumerator SpawnDestroyers()
	{
		yield return new WaitForSeconds (initialStart);
		while(true)
		{
			for(int i = 0; i < hazardCount; i++)
				{
				Vector3 initialLocation = new Vector3( Random.Range(minX,maxX), SpawnZone.y ,Random.Range(MinZ,MaxZ));
					Quaternion rotations = Quaternion.identity;
					Instantiate (Destroyers, initialLocation, rotations);
				}
			yield return new WaitForSeconds (waitBetweenWaves);
		}
	}






}
