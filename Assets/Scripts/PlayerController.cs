﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {


	public float objectSpeed;


	public Text scoreText;

	private int score;

	private Rigidbody rb;


	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.AddForce (movement * objectSpeed);


	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Collect") 
		{
			Destroy (other.gameObject);
			score = score + 10;
			setScore ();

		}
			
	}

	void setScore()
	{
		scoreText.text = "Score: " + score;
	}


}
