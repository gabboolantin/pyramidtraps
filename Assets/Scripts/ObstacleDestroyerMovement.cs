﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleDestroyerMovement : MonoBehaviour {

	public Vector3 initialLocation;
	public float speed; 

	private float yloc;

	void Start()
	{
		initialLocation = transform.position;
		yloc = initialLocation.y;

	}

	// Update is called once per frame
	void Update () {

		yloc = yloc + speed * Time.deltaTime; 
		transform.position = new Vector3(initialLocation.x, yloc, initialLocation.z);
	}
		


}
