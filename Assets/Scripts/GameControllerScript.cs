﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameControllerScript : MonoBehaviour {

	public Text startText,endText;

	public GameObject Player;


	void Start()
	{
		endText.text = "";

		//startText.text = "";
	}
	// Update is called once per frame
	void Update () {
		if (Player.gameObject == null) {
			endText.text = "Nice try! Press R to Reset :D ";
		}


		if (Input.GetKeyDown (KeyCode.R)) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
	}



}
